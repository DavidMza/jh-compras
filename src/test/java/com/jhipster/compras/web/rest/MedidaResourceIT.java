package com.jhipster.compras.web.rest;

import com.jhipster.compras.JhComprasApp;
import com.jhipster.compras.domain.Medida;
import com.jhipster.compras.repository.MedidaRepository;
import com.jhipster.compras.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.jhipster.compras.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MedidaResource} REST controller.
 */
@SpringBootTest(classes = JhComprasApp.class)
public class MedidaResourceIT {

    private static final String DEFAULT_MEDI_NOMB = "AAAAAAAAAA";
    private static final String UPDATED_MEDI_NOMB = "BBBBBBBBBB";

    private static final String DEFAULT_MEDI_DESC = "AAAAAAAAAA";
    private static final String UPDATED_MEDI_DESC = "BBBBBBBBBB";

    @Autowired
    private MedidaRepository medidaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMedidaMockMvc;

    private Medida medida;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MedidaResource medidaResource = new MedidaResource(medidaRepository);
        this.restMedidaMockMvc = MockMvcBuilders.standaloneSetup(medidaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Medida createEntity(EntityManager em) {
        Medida medida = new Medida()
            .mediNomb(DEFAULT_MEDI_NOMB)
            .mediDesc(DEFAULT_MEDI_DESC);
        return medida;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Medida createUpdatedEntity(EntityManager em) {
        Medida medida = new Medida()
            .mediNomb(UPDATED_MEDI_NOMB)
            .mediDesc(UPDATED_MEDI_DESC);
        return medida;
    }

    @BeforeEach
    public void initTest() {
        medida = createEntity(em);
    }

    @Test
    @Transactional
    public void createMedida() throws Exception {
        int databaseSizeBeforeCreate = medidaRepository.findAll().size();

        // Create the Medida
        restMedidaMockMvc.perform(post("/api/medidas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(medida)))
            .andExpect(status().isCreated());

        // Validate the Medida in the database
        List<Medida> medidaList = medidaRepository.findAll();
        assertThat(medidaList).hasSize(databaseSizeBeforeCreate + 1);
        Medida testMedida = medidaList.get(medidaList.size() - 1);
        assertThat(testMedida.getMediNomb()).isEqualTo(DEFAULT_MEDI_NOMB);
        assertThat(testMedida.getMediDesc()).isEqualTo(DEFAULT_MEDI_DESC);
    }

    @Test
    @Transactional
    public void createMedidaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = medidaRepository.findAll().size();

        // Create the Medida with an existing ID
        medida.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMedidaMockMvc.perform(post("/api/medidas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(medida)))
            .andExpect(status().isBadRequest());

        // Validate the Medida in the database
        List<Medida> medidaList = medidaRepository.findAll();
        assertThat(medidaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMedidas() throws Exception {
        // Initialize the database
        medidaRepository.saveAndFlush(medida);

        // Get all the medidaList
        restMedidaMockMvc.perform(get("/api/medidas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(medida.getId().intValue())))
            .andExpect(jsonPath("$.[*].mediNomb").value(hasItem(DEFAULT_MEDI_NOMB.toString())))
            .andExpect(jsonPath("$.[*].mediDesc").value(hasItem(DEFAULT_MEDI_DESC.toString())));
    }
    
    @Test
    @Transactional
    public void getMedida() throws Exception {
        // Initialize the database
        medidaRepository.saveAndFlush(medida);

        // Get the medida
        restMedidaMockMvc.perform(get("/api/medidas/{id}", medida.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(medida.getId().intValue()))
            .andExpect(jsonPath("$.mediNomb").value(DEFAULT_MEDI_NOMB.toString()))
            .andExpect(jsonPath("$.mediDesc").value(DEFAULT_MEDI_DESC.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMedida() throws Exception {
        // Get the medida
        restMedidaMockMvc.perform(get("/api/medidas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMedida() throws Exception {
        // Initialize the database
        medidaRepository.saveAndFlush(medida);

        int databaseSizeBeforeUpdate = medidaRepository.findAll().size();

        // Update the medida
        Medida updatedMedida = medidaRepository.findById(medida.getId()).get();
        // Disconnect from session so that the updates on updatedMedida are not directly saved in db
        em.detach(updatedMedida);
        updatedMedida
            .mediNomb(UPDATED_MEDI_NOMB)
            .mediDesc(UPDATED_MEDI_DESC);

        restMedidaMockMvc.perform(put("/api/medidas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMedida)))
            .andExpect(status().isOk());

        // Validate the Medida in the database
        List<Medida> medidaList = medidaRepository.findAll();
        assertThat(medidaList).hasSize(databaseSizeBeforeUpdate);
        Medida testMedida = medidaList.get(medidaList.size() - 1);
        assertThat(testMedida.getMediNomb()).isEqualTo(UPDATED_MEDI_NOMB);
        assertThat(testMedida.getMediDesc()).isEqualTo(UPDATED_MEDI_DESC);
    }

    @Test
    @Transactional
    public void updateNonExistingMedida() throws Exception {
        int databaseSizeBeforeUpdate = medidaRepository.findAll().size();

        // Create the Medida

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMedidaMockMvc.perform(put("/api/medidas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(medida)))
            .andExpect(status().isBadRequest());

        // Validate the Medida in the database
        List<Medida> medidaList = medidaRepository.findAll();
        assertThat(medidaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMedida() throws Exception {
        // Initialize the database
        medidaRepository.saveAndFlush(medida);

        int databaseSizeBeforeDelete = medidaRepository.findAll().size();

        // Delete the medida
        restMedidaMockMvc.perform(delete("/api/medidas/{id}", medida.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Medida> medidaList = medidaRepository.findAll();
        assertThat(medidaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Medida.class);
        Medida medida1 = new Medida();
        medida1.setId(1L);
        Medida medida2 = new Medida();
        medida2.setId(medida1.getId());
        assertThat(medida1).isEqualTo(medida2);
        medida2.setId(2L);
        assertThat(medida1).isNotEqualTo(medida2);
        medida1.setId(null);
        assertThat(medida1).isNotEqualTo(medida2);
    }
}
