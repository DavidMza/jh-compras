/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { MedidaComponentsPage, MedidaDeleteDialog, MedidaUpdatePage } from './medida.page-object';

const expect = chai.expect;

describe('Medida e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let medidaUpdatePage: MedidaUpdatePage;
  let medidaComponentsPage: MedidaComponentsPage;
  let medidaDeleteDialog: MedidaDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Medidas', async () => {
    await navBarPage.goToEntity('medida');
    medidaComponentsPage = new MedidaComponentsPage();
    await browser.wait(ec.visibilityOf(medidaComponentsPage.title), 5000);
    expect(await medidaComponentsPage.getTitle()).to.eq('jhComprasApp.medida.home.title');
  });

  it('should load create Medida page', async () => {
    await medidaComponentsPage.clickOnCreateButton();
    medidaUpdatePage = new MedidaUpdatePage();
    expect(await medidaUpdatePage.getPageTitle()).to.eq('jhComprasApp.medida.home.createOrEditLabel');
    await medidaUpdatePage.cancel();
  });

  it('should create and save Medidas', async () => {
    const nbButtonsBeforeCreate = await medidaComponentsPage.countDeleteButtons();

    await medidaComponentsPage.clickOnCreateButton();
    await promise.all([medidaUpdatePage.setMediNombInput('mediNomb'), medidaUpdatePage.setMediDescInput('mediDesc')]);
    expect(await medidaUpdatePage.getMediNombInput()).to.eq('mediNomb', 'Expected MediNomb value to be equals to mediNomb');
    expect(await medidaUpdatePage.getMediDescInput()).to.eq('mediDesc', 'Expected MediDesc value to be equals to mediDesc');
    await medidaUpdatePage.save();
    expect(await medidaUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await medidaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Medida', async () => {
    const nbButtonsBeforeDelete = await medidaComponentsPage.countDeleteButtons();
    await medidaComponentsPage.clickOnLastDeleteButton();

    medidaDeleteDialog = new MedidaDeleteDialog();
    expect(await medidaDeleteDialog.getDialogTitle()).to.eq('jhComprasApp.medida.delete.question');
    await medidaDeleteDialog.clickOnConfirmButton();

    expect(await medidaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
