import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class MedidaComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-medida div table .btn-danger'));
  title = element.all(by.css('jhi-medida div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class MedidaUpdatePage {
  pageTitle = element(by.id('jhi-medida-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  mediNombInput = element(by.id('field_mediNomb'));
  mediDescInput = element(by.id('field_mediDesc'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setMediNombInput(mediNomb) {
    await this.mediNombInput.sendKeys(mediNomb);
  }

  async getMediNombInput() {
    return await this.mediNombInput.getAttribute('value');
  }

  async setMediDescInput(mediDesc) {
    await this.mediDescInput.sendKeys(mediDesc);
  }

  async getMediDescInput() {
    return await this.mediDescInput.getAttribute('value');
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class MedidaDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-medida-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-medida'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
