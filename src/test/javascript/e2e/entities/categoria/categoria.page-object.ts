import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class CategoriaComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-categoria div table .btn-danger'));
  title = element.all(by.css('jhi-categoria div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CategoriaUpdatePage {
  pageTitle = element(by.id('jhi-categoria-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  cateNombInput = element(by.id('field_cateNomb'));
  cateDescInput = element(by.id('field_cateDesc'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setCateNombInput(cateNomb) {
    await this.cateNombInput.sendKeys(cateNomb);
  }

  async getCateNombInput() {
    return await this.cateNombInput.getAttribute('value');
  }

  async setCateDescInput(cateDesc) {
    await this.cateDescInput.sendKeys(cateDesc);
  }

  async getCateDescInput() {
    return await this.cateDescInput.getAttribute('value');
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CategoriaDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-categoria-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-categoria'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
