/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { MarcaComponentsPage, MarcaDeleteDialog, MarcaUpdatePage } from './marca.page-object';

const expect = chai.expect;

describe('Marca e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let marcaUpdatePage: MarcaUpdatePage;
  let marcaComponentsPage: MarcaComponentsPage;
  let marcaDeleteDialog: MarcaDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Marcas', async () => {
    await navBarPage.goToEntity('marca');
    marcaComponentsPage = new MarcaComponentsPage();
    await browser.wait(ec.visibilityOf(marcaComponentsPage.title), 5000);
    expect(await marcaComponentsPage.getTitle()).to.eq('jhComprasApp.marca.home.title');
  });

  it('should load create Marca page', async () => {
    await marcaComponentsPage.clickOnCreateButton();
    marcaUpdatePage = new MarcaUpdatePage();
    expect(await marcaUpdatePage.getPageTitle()).to.eq('jhComprasApp.marca.home.createOrEditLabel');
    await marcaUpdatePage.cancel();
  });

  it('should create and save Marcas', async () => {
    const nbButtonsBeforeCreate = await marcaComponentsPage.countDeleteButtons();

    await marcaComponentsPage.clickOnCreateButton();
    await promise.all([marcaUpdatePage.setMarcNombInput('marcNomb'), marcaUpdatePage.setMarcDescInput('marcDesc')]);
    expect(await marcaUpdatePage.getMarcNombInput()).to.eq('marcNomb', 'Expected MarcNomb value to be equals to marcNomb');
    expect(await marcaUpdatePage.getMarcDescInput()).to.eq('marcDesc', 'Expected MarcDesc value to be equals to marcDesc');
    await marcaUpdatePage.save();
    expect(await marcaUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await marcaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Marca', async () => {
    const nbButtonsBeforeDelete = await marcaComponentsPage.countDeleteButtons();
    await marcaComponentsPage.clickOnLastDeleteButton();

    marcaDeleteDialog = new MarcaDeleteDialog();
    expect(await marcaDeleteDialog.getDialogTitle()).to.eq('jhComprasApp.marca.delete.question');
    await marcaDeleteDialog.clickOnConfirmButton();

    expect(await marcaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
