import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class MarcaComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-marca div table .btn-danger'));
  title = element.all(by.css('jhi-marca div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class MarcaUpdatePage {
  pageTitle = element(by.id('jhi-marca-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  marcNombInput = element(by.id('field_marcNomb'));
  marcDescInput = element(by.id('field_marcDesc'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setMarcNombInput(marcNomb) {
    await this.marcNombInput.sendKeys(marcNomb);
  }

  async getMarcNombInput() {
    return await this.marcNombInput.getAttribute('value');
  }

  async setMarcDescInput(marcDesc) {
    await this.marcDescInput.sendKeys(marcDesc);
  }

  async getMarcDescInput() {
    return await this.marcDescInput.getAttribute('value');
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class MarcaDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-marca-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-marca'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
