import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class ProductoComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-producto div table .btn-danger'));
  title = element.all(by.css('jhi-producto div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ProductoUpdatePage {
  pageTitle = element(by.id('jhi-producto-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  prodNombInput = element(by.id('field_prodNomb'));
  prodDescInput = element(by.id('field_prodDesc'));
  prodPackInput = element(by.id('field_prodPack'));
  medidaSelect = element(by.id('field_medida'));
  marcaSelect = element(by.id('field_marca'));
  categoriaSelect = element(by.id('field_categoria'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setProdNombInput(prodNomb) {
    await this.prodNombInput.sendKeys(prodNomb);
  }

  async getProdNombInput() {
    return await this.prodNombInput.getAttribute('value');
  }

  async setProdDescInput(prodDesc) {
    await this.prodDescInput.sendKeys(prodDesc);
  }

  async getProdDescInput() {
    return await this.prodDescInput.getAttribute('value');
  }

  async setProdPackInput(prodPack) {
    await this.prodPackInput.sendKeys(prodPack);
  }

  async getProdPackInput() {
    return await this.prodPackInput.getAttribute('value');
  }

  async medidaSelectLastOption(timeout?: number) {
    await this.medidaSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async medidaSelectOption(option) {
    await this.medidaSelect.sendKeys(option);
  }

  getMedidaSelect(): ElementFinder {
    return this.medidaSelect;
  }

  async getMedidaSelectedOption() {
    return await this.medidaSelect.element(by.css('option:checked')).getText();
  }

  async marcaSelectLastOption(timeout?: number) {
    await this.marcaSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async marcaSelectOption(option) {
    await this.marcaSelect.sendKeys(option);
  }

  getMarcaSelect(): ElementFinder {
    return this.marcaSelect;
  }

  async getMarcaSelectedOption() {
    return await this.marcaSelect.element(by.css('option:checked')).getText();
  }

  async categoriaSelectLastOption(timeout?: number) {
    await this.categoriaSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async categoriaSelectOption(option) {
    await this.categoriaSelect.sendKeys(option);
  }

  getCategoriaSelect(): ElementFinder {
    return this.categoriaSelect;
  }

  async getCategoriaSelectedOption() {
    return await this.categoriaSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ProductoDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-producto-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-producto'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
