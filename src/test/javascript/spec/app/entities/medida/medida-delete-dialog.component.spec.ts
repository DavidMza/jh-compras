/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { JhComprasTestModule } from '../../../test.module';
import { MedidaDeleteDialogComponent } from 'app/entities/medida/medida-delete-dialog.component';
import { MedidaService } from 'app/entities/medida/medida.service';

describe('Component Tests', () => {
  describe('Medida Management Delete Component', () => {
    let comp: MedidaDeleteDialogComponent;
    let fixture: ComponentFixture<MedidaDeleteDialogComponent>;
    let service: MedidaService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhComprasTestModule],
        declarations: [MedidaDeleteDialogComponent]
      })
        .overrideTemplate(MedidaDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MedidaDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MedidaService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
