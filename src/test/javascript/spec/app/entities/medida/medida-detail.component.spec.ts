/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhComprasTestModule } from '../../../test.module';
import { MedidaDetailComponent } from 'app/entities/medida/medida-detail.component';
import { Medida } from 'app/shared/model/medida.model';

describe('Component Tests', () => {
  describe('Medida Management Detail Component', () => {
    let comp: MedidaDetailComponent;
    let fixture: ComponentFixture<MedidaDetailComponent>;
    const route = ({ data: of({ medida: new Medida(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhComprasTestModule],
        declarations: [MedidaDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MedidaDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MedidaDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.medida).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
