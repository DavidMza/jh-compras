package com.jhipster.compras.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Categoria.
 */
@Entity
@Table(name = "categoria")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Categoria implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "cate_nomb")
    private String cateNomb;

    @Column(name = "cate_desc")
    private String cateDesc;

    @OneToMany(mappedBy = "categoria")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Producto> prodCates = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCateNomb() {
        return cateNomb;
    }

    public Categoria cateNomb(String cateNomb) {
        this.cateNomb = cateNomb;
        return this;
    }

    public void setCateNomb(String cateNomb) {
        this.cateNomb = cateNomb;
    }

    public String getCateDesc() {
        return cateDesc;
    }

    public Categoria cateDesc(String cateDesc) {
        this.cateDesc = cateDesc;
        return this;
    }

    public void setCateDesc(String cateDesc) {
        this.cateDesc = cateDesc;
    }

    public Set<Producto> getProdCates() {
        return prodCates;
    }

    public Categoria prodCates(Set<Producto> productos) {
        this.prodCates = productos;
        return this;
    }

    public Categoria addProdCate(Producto producto) {
        this.prodCates.add(producto);
        producto.setCategoria(this);
        return this;
    }

    public Categoria removeProdCate(Producto producto) {
        this.prodCates.remove(producto);
        producto.setCategoria(null);
        return this;
    }

    public void setProdCates(Set<Producto> productos) {
        this.prodCates = productos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Categoria)) {
            return false;
        }
        return id != null && id.equals(((Categoria) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Categoria{" +
            "id=" + getId() +
            ", cateNomb='" + getCateNomb() + "'" +
            ", cateDesc='" + getCateDesc() + "'" +
            "}";
    }
}
