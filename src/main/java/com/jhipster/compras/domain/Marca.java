package com.jhipster.compras.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Marca.
 */
@Entity
@Table(name = "marca")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Marca implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "marc_nomb")
    private String marcNomb;

    @Column(name = "marc_desc")
    private String marcDesc;

    @OneToMany(mappedBy = "marca")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Producto> prodMarcs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMarcNomb() {
        return marcNomb;
    }

    public Marca marcNomb(String marcNomb) {
        this.marcNomb = marcNomb;
        return this;
    }

    public void setMarcNomb(String marcNomb) {
        this.marcNomb = marcNomb;
    }

    public String getMarcDesc() {
        return marcDesc;
    }

    public Marca marcDesc(String marcDesc) {
        this.marcDesc = marcDesc;
        return this;
    }

    public void setMarcDesc(String marcDesc) {
        this.marcDesc = marcDesc;
    }

    public Set<Producto> getProdMarcs() {
        return prodMarcs;
    }

    public Marca prodMarcs(Set<Producto> productos) {
        this.prodMarcs = productos;
        return this;
    }

    public Marca addProdMarc(Producto producto) {
        this.prodMarcs.add(producto);
        producto.setMarca(this);
        return this;
    }

    public Marca removeProdMarc(Producto producto) {
        this.prodMarcs.remove(producto);
        producto.setMarca(null);
        return this;
    }

    public void setProdMarcs(Set<Producto> productos) {
        this.prodMarcs = productos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Marca)) {
            return false;
        }
        return id != null && id.equals(((Marca) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Marca{" +
            "id=" + getId() +
            ", marcNomb='" + getMarcNomb() + "'" +
            ", marcDesc='" + getMarcDesc() + "'" +
            "}";
    }
}
