package com.jhipster.compras.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Producto.
 */
@Entity
@Table(name = "producto")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "prod_nomb")
    private String prodNomb;

    @Column(name = "prod_desc")
    private String prodDesc;

    @Column(name = "prod_pack")
    private Double prodPack;

    @ManyToOne
    @JsonIgnoreProperties("productos")
    private Medida medida;

    @ManyToOne
    @JsonIgnoreProperties("productos")
    private Marca marca;

    @ManyToOne
    @JsonIgnoreProperties("productos")
    private Categoria categoria;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProdNomb() {
        return prodNomb;
    }

    public Producto prodNomb(String prodNomb) {
        this.prodNomb = prodNomb;
        return this;
    }

    public void setProdNomb(String prodNomb) {
        this.prodNomb = prodNomb;
    }

    public String getProdDesc() {
        return prodDesc;
    }

    public Producto prodDesc(String prodDesc) {
        this.prodDesc = prodDesc;
        return this;
    }

    public void setProdDesc(String prodDesc) {
        this.prodDesc = prodDesc;
    }

    public Double getProdPack() {
        return prodPack;
    }

    public Producto prodPack(Double prodPack) {
        this.prodPack = prodPack;
        return this;
    }

    public void setProdPack(Double prodPack) {
        this.prodPack = prodPack;
    }

    public Medida getMedida() {
        return medida;
    }

    public Producto medida(Medida medida) {
        this.medida = medida;
        return this;
    }

    public void setMedida(Medida medida) {
        this.medida = medida;
    }

    public Marca getMarca() {
        return marca;
    }

    public Producto marca(Marca marca) {
        this.marca = marca;
        return this;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public Producto categoria(Categoria categoria) {
        this.categoria = categoria;
        return this;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Producto)) {
            return false;
        }
        return id != null && id.equals(((Producto) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Producto{" +
            "id=" + getId() +
            ", prodNomb='" + getProdNomb() + "'" +
            ", prodDesc='" + getProdDesc() + "'" +
            ", prodPack=" + getProdPack() +
            "}";
    }
}
