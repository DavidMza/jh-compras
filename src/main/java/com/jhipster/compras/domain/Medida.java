package com.jhipster.compras.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Medida.
 */
@Entity
@Table(name = "medida")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Medida implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "medi_nomb")
    private String mediNomb;

    @Column(name = "medi_desc")
    private String mediDesc;

    @OneToMany(mappedBy = "medida")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Producto> prodMedis = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMediNomb() {
        return mediNomb;
    }

    public Medida mediNomb(String mediNomb) {
        this.mediNomb = mediNomb;
        return this;
    }

    public void setMediNomb(String mediNomb) {
        this.mediNomb = mediNomb;
    }

    public String getMediDesc() {
        return mediDesc;
    }

    public Medida mediDesc(String mediDesc) {
        this.mediDesc = mediDesc;
        return this;
    }

    public void setMediDesc(String mediDesc) {
        this.mediDesc = mediDesc;
    }

    public Set<Producto> getProdMedis() {
        return prodMedis;
    }

    public Medida prodMedis(Set<Producto> productos) {
        this.prodMedis = productos;
        return this;
    }

    public Medida addProdMedi(Producto producto) {
        this.prodMedis.add(producto);
        producto.setMedida(this);
        return this;
    }

    public Medida removeProdMedi(Producto producto) {
        this.prodMedis.remove(producto);
        producto.setMedida(null);
        return this;
    }

    public void setProdMedis(Set<Producto> productos) {
        this.prodMedis = productos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Medida)) {
            return false;
        }
        return id != null && id.equals(((Medida) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Medida{" +
            "id=" + getId() +
            ", mediNomb='" + getMediNomb() + "'" +
            ", mediDesc='" + getMediDesc() + "'" +
            "}";
    }
}
