package com.jhipster.compras.web.rest;

import com.jhipster.compras.domain.Medida;
import com.jhipster.compras.repository.MedidaRepository;
import com.jhipster.compras.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.jhipster.compras.domain.Medida}.
 */
@RestController
@RequestMapping("/api")
public class MedidaResource {

    private final Logger log = LoggerFactory.getLogger(MedidaResource.class);

    private static final String ENTITY_NAME = "medida";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MedidaRepository medidaRepository;

    public MedidaResource(MedidaRepository medidaRepository) {
        this.medidaRepository = medidaRepository;
    }

    /**
     * {@code POST  /medidas} : Create a new medida.
     *
     * @param medida the medida to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new medida, or with status {@code 400 (Bad Request)} if the medida has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/medidas")
    public ResponseEntity<Medida> createMedida(@RequestBody Medida medida) throws URISyntaxException {
        log.debug("REST request to save Medida : {}", medida);
        if (medida.getId() != null) {
            throw new BadRequestAlertException("A new medida cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Medida result = medidaRepository.save(medida);
        return ResponseEntity.created(new URI("/api/medidas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /medidas} : Updates an existing medida.
     *
     * @param medida the medida to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated medida,
     * or with status {@code 400 (Bad Request)} if the medida is not valid,
     * or with status {@code 500 (Internal Server Error)} if the medida couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/medidas")
    public ResponseEntity<Medida> updateMedida(@RequestBody Medida medida) throws URISyntaxException {
        log.debug("REST request to update Medida : {}", medida);
        if (medida.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Medida result = medidaRepository.save(medida);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, medida.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /medidas} : get all the medidas.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of medidas in body.
     */
    @GetMapping("/medidas")
    public ResponseEntity<List<Medida>> getAllMedidas(Pageable pageable) {
        log.debug("REST request to get a page of Medidas");
        Page<Medida> page = medidaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /medidas/:id} : get the "id" medida.
     *
     * @param id the id of the medida to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the medida, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/medidas/{id}")
    public ResponseEntity<Medida> getMedida(@PathVariable Long id) {
        log.debug("REST request to get Medida : {}", id);
        Optional<Medida> medida = medidaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(medida);
    }

    /**
     * {@code DELETE  /medidas/:id} : delete the "id" medida.
     *
     * @param id the id of the medida to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/medidas/{id}")
    public ResponseEntity<Void> deleteMedida(@PathVariable Long id) {
        log.debug("REST request to delete Medida : {}", id);
        medidaRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
