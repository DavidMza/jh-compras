/**
 * View Models used by Spring MVC REST controllers.
 */
package com.jhipster.compras.web.rest.vm;
