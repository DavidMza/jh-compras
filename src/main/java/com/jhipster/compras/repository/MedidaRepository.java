package com.jhipster.compras.repository;

import com.jhipster.compras.domain.Medida;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Medida entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MedidaRepository extends JpaRepository<Medida, Long> {

}
