import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { JhComprasSharedModule } from 'app/shared';
import {
  MedidaComponent,
  MedidaDetailComponent,
  MedidaUpdateComponent,
  MedidaDeletePopupComponent,
  MedidaDeleteDialogComponent,
  medidaRoute,
  medidaPopupRoute
} from './';

const ENTITY_STATES = [...medidaRoute, ...medidaPopupRoute];

@NgModule({
  imports: [JhComprasSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [MedidaComponent, MedidaDetailComponent, MedidaUpdateComponent, MedidaDeleteDialogComponent, MedidaDeletePopupComponent],
  entryComponents: [MedidaComponent, MedidaUpdateComponent, MedidaDeleteDialogComponent, MedidaDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhComprasMedidaModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
