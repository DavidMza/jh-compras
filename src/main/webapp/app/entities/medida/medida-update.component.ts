import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IMedida, Medida } from 'app/shared/model/medida.model';
import { MedidaService } from './medida.service';

@Component({
  selector: 'jhi-medida-update',
  templateUrl: './medida-update.component.html'
})
export class MedidaUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    mediNomb: [],
    mediDesc: []
  });

  constructor(protected medidaService: MedidaService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ medida }) => {
      this.updateForm(medida);
    });
  }

  updateForm(medida: IMedida) {
    this.editForm.patchValue({
      id: medida.id,
      mediNomb: medida.mediNomb,
      mediDesc: medida.mediDesc
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const medida = this.createFromForm();
    if (medida.id !== undefined) {
      this.subscribeToSaveResponse(this.medidaService.update(medida));
    } else {
      this.subscribeToSaveResponse(this.medidaService.create(medida));
    }
  }

  private createFromForm(): IMedida {
    return {
      ...new Medida(),
      id: this.editForm.get(['id']).value,
      mediNomb: this.editForm.get(['mediNomb']).value,
      mediDesc: this.editForm.get(['mediDesc']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMedida>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
