import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMedida } from 'app/shared/model/medida.model';

@Component({
  selector: 'jhi-medida-detail',
  templateUrl: './medida-detail.component.html'
})
export class MedidaDetailComponent implements OnInit {
  medida: IMedida;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ medida }) => {
      this.medida = medida;
    });
  }

  previousState() {
    window.history.back();
  }
}
