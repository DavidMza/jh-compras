import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMedida } from 'app/shared/model/medida.model';
import { MedidaService } from './medida.service';

@Component({
  selector: 'jhi-medida-delete-dialog',
  templateUrl: './medida-delete-dialog.component.html'
})
export class MedidaDeleteDialogComponent {
  medida: IMedida;

  constructor(protected medidaService: MedidaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.medidaService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'medidaListModification',
        content: 'Deleted an medida'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-medida-delete-popup',
  template: ''
})
export class MedidaDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ medida }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MedidaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.medida = medida;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/medida', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/medida', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
