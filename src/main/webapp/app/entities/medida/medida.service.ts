import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMedida } from 'app/shared/model/medida.model';

type EntityResponseType = HttpResponse<IMedida>;
type EntityArrayResponseType = HttpResponse<IMedida[]>;

@Injectable({ providedIn: 'root' })
export class MedidaService {
  public resourceUrl = SERVER_API_URL + 'api/medidas';

  constructor(protected http: HttpClient) {}

  create(medida: IMedida): Observable<EntityResponseType> {
    return this.http.post<IMedida>(this.resourceUrl, medida, { observe: 'response' });
  }

  update(medida: IMedida): Observable<EntityResponseType> {
    return this.http.put<IMedida>(this.resourceUrl, medida, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMedida>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMedida[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
