import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Medida } from 'app/shared/model/medida.model';
import { MedidaService } from './medida.service';
import { MedidaComponent } from './medida.component';
import { MedidaDetailComponent } from './medida-detail.component';
import { MedidaUpdateComponent } from './medida-update.component';
import { MedidaDeletePopupComponent } from './medida-delete-dialog.component';
import { IMedida } from 'app/shared/model/medida.model';

@Injectable({ providedIn: 'root' })
export class MedidaResolve implements Resolve<IMedida> {
  constructor(private service: MedidaService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMedida> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Medida>) => response.ok),
        map((medida: HttpResponse<Medida>) => medida.body)
      );
    }
    return of(new Medida());
  }
}

export const medidaRoute: Routes = [
  {
    path: '',
    component: MedidaComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'jhComprasApp.medida.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MedidaDetailComponent,
    resolve: {
      medida: MedidaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhComprasApp.medida.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MedidaUpdateComponent,
    resolve: {
      medida: MedidaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhComprasApp.medida.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MedidaUpdateComponent,
    resolve: {
      medida: MedidaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhComprasApp.medida.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const medidaPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MedidaDeletePopupComponent,
    resolve: {
      medida: MedidaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhComprasApp.medida.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
