import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { JhComprasSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [JhComprasSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [JhComprasSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhComprasSharedModule {
  static forRoot() {
    return {
      ngModule: JhComprasSharedModule
    };
  }
}
