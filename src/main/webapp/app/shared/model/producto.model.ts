import { IMedida } from 'app/shared/model/medida.model';
import { IMarca } from 'app/shared/model/marca.model';
import { ICategoria } from 'app/shared/model/categoria.model';

export interface IProducto {
  id?: number;
  prodNomb?: string;
  prodDesc?: string;
  prodPack?: number;
  medida?: IMedida;
  marca?: IMarca;
  categoria?: ICategoria;
}

export class Producto implements IProducto {
  constructor(
    public id?: number,
    public prodNomb?: string,
    public prodDesc?: string,
    public prodPack?: number,
    public medida?: IMedida,
    public marca?: IMarca,
    public categoria?: ICategoria
  ) {}
}
