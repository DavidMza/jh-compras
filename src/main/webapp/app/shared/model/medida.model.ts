import { IProducto } from 'app/shared/model/producto.model';

export interface IMedida {
  id?: number;
  mediNomb?: string;
  mediDesc?: string;
  prodMedis?: IProducto[];
}

export class Medida implements IMedida {
  constructor(public id?: number, public mediNomb?: string, public mediDesc?: string, public prodMedis?: IProducto[]) {}
}
