import { IProducto } from 'app/shared/model/producto.model';

export interface IMarca {
  id?: number;
  marcNomb?: string;
  marcDesc?: string;
  prodMarcs?: IProducto[];
}

export class Marca implements IMarca {
  constructor(public id?: number, public marcNomb?: string, public marcDesc?: string, public prodMarcs?: IProducto[]) {}
}
