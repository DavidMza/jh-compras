import { IProducto } from 'app/shared/model/producto.model';

export interface ICategoria {
  id?: number;
  cateNomb?: string;
  cateDesc?: string;
  prodCates?: IProducto[];
}

export class Categoria implements ICategoria {
  constructor(public id?: number, public cateNomb?: string, public cateDesc?: string, public prodCates?: IProducto[]) {}
}
